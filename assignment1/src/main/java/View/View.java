package View;
import javax.swing.*;

public class View {
    private JFrame frame = new JFrame("Polynomial Calculator");

    private JPanel panelPoly1 = new JPanel();
    private JPanel panelPoly2 = new JPanel();
    private JPanel panelPoly = new JPanel();
    private JPanel panelOp = new JPanel();
    private JPanel panelRes = new JPanel();
    private JPanel panelRes1 = new JPanel();
    private JPanel panelRes2 = new JPanel();
    private JPanel p = new JPanel();

    private JButton sumButton = new JButton("+");
    private JButton substrButton = new JButton("-");
    private JButton mulButton = new JButton("*");
    private JButton divButton = new JButton("/");
    private JButton derivButton = new JButton("A'");
    private JButton integrButton = new JButton("\u222BA");
    private JButton clear = new JButton("Clear");

    private JTextField firstPoly = new JTextField(50);
    private JTextField secondPoly = new JTextField(50);
    private JTextField resultPoly = new JTextField(53);
    private JTextField remainderPoly = new JTextField(50);

    private JLabel polyA = new JLabel("Polynomial A: ");
    private JLabel polyB = new JLabel("Polynomial B: ");
    private JLabel result = new JLabel("Result: ");
    private JLabel remainder = new JLabel("Remainder: ");

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanelPoly1() {
        return panelPoly1;
    }

    public void setPanelPoly1(JPanel panelPoly1) {
        this.panelPoly1 = panelPoly1;
    }

    public JPanel getPanelPoly2() {
        return panelPoly2;
    }

    public void setPanelPoly2(JPanel panelPoly2) {
        this.panelPoly2 = panelPoly2;
    }

    public JPanel getPanelPoly() {
        return panelPoly;
    }

    public void setPanelPoly(JPanel panelPoly) {
        this.panelPoly = panelPoly;
    }

    public JPanel getPanelOp() {
        return panelOp;
    }

    public void setPanelOp(JPanel panelOp) {
        this.panelOp = panelOp;
    }

    public JPanel getPanelRes() {
        return panelRes;
    }

    public void setPanelRes(JPanel panelRes) {
        this.panelRes = panelRes;
    }

    public JPanel getPanelRes1() {
        return panelRes1;
    }

    public void setPanelRes1(JPanel panelRes1) {
        this.panelRes1 = panelRes1;
    }

    public JPanel getPanelRes2() {
        return panelRes2;
    }

    public void setPanelRes2(JPanel panelRes2) {
        this.panelRes2 = panelRes2;
    }

    public JButton getSumButton() {
        return sumButton;
    }

    public void setSumButton(JButton sumButton) {
        this.sumButton = sumButton;
    }

    public JButton getSubstrButton() {
        return substrButton;
    }

    public void setSubstrButton(JButton substrButton) {
        this.substrButton = substrButton;
    }

    public JButton getMulButton() {
        return mulButton;
    }

    public void setMulButton(JButton mulButton) {
        this.mulButton = mulButton;
    }

    public JButton getDivButton() {
        return divButton;
    }

    public void setDivButton(JButton divButton) {
        this.divButton = divButton;
    }

    public JButton getDerivButton() {
        return derivButton;
    }

    public void setDerivButton(JButton derivButton) {
        this.derivButton = derivButton;
    }

    public JButton getIntegrButton() {
        return integrButton;
    }

    public void setIntegrButton(JButton integrButton) {
        this.integrButton = integrButton;
    }

    public JButton getClear() {
        return clear;
    }

    public void setClear(JButton clear) {
        this.clear = clear;
    }

    public JTextField getFirstPoly() {
        return firstPoly;
    }

    public void setFirstPoly(JTextField firstPoly) {
        this.firstPoly = firstPoly;
    }

    public JTextField getSecondPoly() {
        return secondPoly;
    }

    public void setSecondPoly(JTextField secondPoly) {
        this.secondPoly = secondPoly;
    }

    public JTextField getResultPoly() {
        return resultPoly;
    }

    public void setResultPoly(JTextField resultPoly) {
        this.resultPoly = resultPoly;
    }

    public JTextField getRemainderPoly() {
        return remainderPoly;
    }

    public void setRemainderPoly(JTextField remainderPoly) {
        this.remainderPoly = remainderPoly;
    }

    public JLabel getPolyA() {
        return polyA;
    }

    public void setPolyA(JLabel polyA) {
        this.polyA = polyA;
    }

    public JLabel getPolyB() {
        return polyB;
    }

    public void setPolyB(JLabel polyB) {
        this.polyB = polyB;
    }

    public JLabel getResult() {
        return result;
    }

    public void setResult(JLabel result) {
        this.result = result;
    }

    public JLabel getRemainder() {
        return remainder;
    }

    public void setRemainder(JLabel remainder) {
        this.remainder = remainder;
    }

    public View(String name) {

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,250);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        panelPoly1.add(polyA);
        panelPoly1.add(firstPoly);
        panelPoly2.add(polyB);
        panelPoly2.add(secondPoly);
        panelPoly.add(panelPoly1);
        panelPoly.add(panelPoly2);

        panelOp.add(sumButton);
        panelOp.add(substrButton);
        panelOp.add(mulButton);
        panelOp.add(divButton);
        panelOp.add(derivButton);
        panelOp.add(integrButton);

        panelRes1.add(result);
        panelRes1.add(resultPoly);
        panelRes2.add(remainder);
        panelRes2.add(remainderPoly);
        panelRes.add(panelRes1);
        panelRes.add(panelRes2);

        panelRes.add(clear);

        panelPoly.setLayout(new BoxLayout(panelPoly, BoxLayout.Y_AXIS));
        panelRes.setLayout(new BoxLayout(panelRes, BoxLayout.Y_AXIS));

        p.add(panelPoly);
        p.add(panelOp);
        p.add(panelRes);

        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        frame.setContentPane(p);
        frame.setVisible(true);
    }

}

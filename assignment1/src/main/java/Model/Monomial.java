package Model;

import java.text.DecimalFormat;
import java.util.Objects;
import java.math.RoundingMode;

public class Monomial {
    private double coef;
    private int pow;
    private static DecimalFormat DF2 = new DecimalFormat("#.##");

    public Monomial(){
    }

    public Monomial(double coef, int pow) {
        this.coef = coef;
        this.pow = pow;
    }

    public double getCoef() {
        return coef;
    }

    public void setCoef(double coef) {
        this.coef = Double.parseDouble(DF2.format(coef));
    }

    public int getPow() {
        return pow;
    }

    public void setPow(int pow) {
        this.pow = pow;
    }

    @Override
    public String toString() {
        return "Monomial{" +
                "coef=" + coef +
                ", pow=" + pow +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monomial)) return false;
        Monomial monomial = (Monomial) o;
        return Double.compare(monomial.getCoef(), getCoef()) == 0 &&
                getPow() == monomial.getPow();
    }

    public int compareTo(Object o){
        Monomial m = (Monomial) o;
        if(this.getPow() == m.getPow()){
            return 0;
        }
        else{
            if(this.getPow() < m.getPow()){
                return -1;
            }
            else
                return 1;
        }
    }


}
